const SentryCliPlugin = require('@sentry/webpack-plugin');

// vue.config.js
module.exports = {
    configureWebpack: config => {
      if (process.env.NODE_ENV === 'production') {
        // 为生产环境修改配置...

        return {
            plugins: [
                new SentryCliPlugin({
                    release: require('./package.json').version,
                    include: './dist'
                }),
            ]
        }
      } else {
        // 为开发环境修改配置...
      }
    }
}