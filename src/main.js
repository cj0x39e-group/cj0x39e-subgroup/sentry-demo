import Vue from 'vue'
import App from './App.vue'

import * as Sentry from '@sentry/browser';
import * as Integrations from '@sentry/integrations';

Sentry.init({
  release: require('../package.json').version,
  dsn: 'http://606e2fcdbc3445889982dafe6478119e@localhost:9000/3',
  integrations: [new Integrations.Vue({Vue, attachProps: true})],
});

Vue.config.productionTip = false

new Vue({
  render: h => h(App),
}).$mount('#app')

// eslint-disable-next-line no-undef
myUndefinedFunction();

docker run -d  \
-p 22:22 \
-p 80:80 \
-p 443:443 \
--restart always  \
--name gitlab gitlab/gitlab-ce:latest